function [ y ] = D2psi1( x,k )
%family of funtions, basis for the space
%   fisrt case: polynomials
if k>1
    y=k*(k-1)*x.^(k-2);
else
    y=0;
end
end

