function [ y ] = D2psi2( x,k )
%family of funtions, basis for the space
%   fisrt case: polynomials
if k==0
    y=0;
elseif k==1
    y=-cos(x);
elseif k==2
    y=-1.0i*sin(x);
elseif k==3
    y=-4*cos(2*x);
elseif k==4
    y=-4.0i*sin(2*x);
else
    y=-9*cos(3*x);
end
end

