function [ y ] = D2psi4( x,k )
%family of funtions, basis for the space
%   fisrt case: polynomials
if k==0
    y=0;
elseif k==1
    y=-cos(x)-1.0i*sin(x);
elseif k==2
    y=-cos(x)+1.0i*sin(x);
elseif k==3
    y=-4*cos(2*x)-4.0i*sin(2*x);
elseif k==4
    y=-4*cos(2*x)+4.0i*sin(2*x);
end
end

