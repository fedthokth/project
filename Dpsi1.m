function [ y ] = Dpsi1( x,k )
%family of funtions, basis for the space
%   fisrt case: polynomials
if k>0
    y=k*x.^(k-1);
else
    y=0;
end
end

