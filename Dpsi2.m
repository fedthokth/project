function [ y ] = Dpsi2( x,k )
%family of funtions, basis for the space
%   fisrt case: polynomials
if k==0
    y=0;
elseif k==1
    y=-sin(x);
elseif k==2
    y=1.0i*cos(x);
elseif k==3
    y=-2*sin(2*x);
elseif k==4
    y=2.0i*cos(2*x);
else
    y=-3*sin(3*x);
end
end

