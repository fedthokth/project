function [ y ] = Dpsi3( x,k )
%family of funtions, basis for the space
%   fisrt case: polynomials
if k==0
    y=0;
elseif k>0 && k<=3
    y=psi3(x,k+1)*(3*x^2+2*x+1);
end
end

