function [ y ] = Dpsi4( x,k )
%family of funtions, basis for the space
%   fisrt case: polynomials
if k==0
    y=0;
elseif k==1
    y=-sin(x)+1.0i*cos(x);
elseif k==2
    y=-sin(x)-1.0i*cos(x);
elseif k==3
    y=-2*sin(2*x)+2.0i*cos(2*x);
elseif k==4
    y=-2*sin(2*x)-2.0i*cos(2*x);
end
end

