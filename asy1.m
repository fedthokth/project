close all
clear all

fg=@(x,om) cos(x).*exp(om*(x.^2+x)*1.0i); 
f=@(x) cos(x);
g=@(x) x.^2+x;
gp1=@(x) 2*x+1;

s1=@(x) f(x)/gp1(x);
s2=@(x) -(2*cos(x)+sin(x)+2*x*sin(x))/((1+2*x)^3);
s3=@(x) ((11-4*x*(1+x))*cos(x)+6*(1+2*x)*sin(x))/((1+2*x)^5);
s4=@(x) (12*(-9+4*x*(1+x))*cos(x)+(1+2*x)*sin(x))/((1+2*x)^7);
s5=@(x) ((1501+8*x*(1+x)*(-89+2*x*(1+x)))*cos(x)-20*(1+2*x)*(-41+4*x*(1+x))*sin(x))/((1+2*x)^9);
s6=@(x) -(30*(897+8*x*(1+x)*(-55+2*x*(1+x)))*cos(x)+(1+2*x)*(14701+8*x*(1+x)*(-209+2*x*(1+x)))*sin(x))/(1+2*x)^11;

% s7=@(x) ()/((1+2*x)^13)


I=[0,1];
a=I(1);
b=I(2);



i=0;

for om=1

i=i+1;
% om=10;
om
% fg1=@(x) fg(x,om);
sol1=quadgk(@(x) fg(x,om),0,1,'AbsTol',1e-15,'RelTol',1e-15);

solasy1=-(s1(b)*exp(om*g(b)*1.0i) -s1(a)*exp(om*g(a)*1.0i))/(-(om*1.0i));

solasy2=solasy1-(s2(b)*exp(om*g(b)*1.0i)-s2(a)*exp(om*g(a)*1.0i))/(-(om*1.0i)^2);

solasy3=solasy2-(s3(b)*exp(om*g(b)*1.0i)-s3(a)*exp(om*g(a)*1.0i))/(-(om*1.0i)^3);

solasy4=solasy3-(s4(b)*exp(om*g(b)*1.0i)-s4(a)*exp(om*g(a)*1.0i))/(-(om*1.0i)^4);

solasy5=solasy4-(s5(b)*exp(om*g(b)*1.0i)-s5(a)*exp(om*g(a)*1.0i))/(-(om*1.0i)^5);

solasy6=solasy5-(s6(b)*exp(om*g(b)*1.0i)-s6(a)*exp(om*g(a)*1.0i))/(-(om*1.0i)^6);

err1(i)=sol1-solasy1
err2(i)=sol1-solasy2
err3(i)=sol1-solasy3
err4(i)=sol1-solasy4
err5(i)=sol1-solasy5
err6(i)=sol1-solasy6
%%%%%%%%%%%%%% just checking the error from the asymptotic expansion at the
%%%%%%%%%%%%%% start, to make the curve in the order start from the right
%%%%%%%%%%%%%% point. I printed the plot in asympt1.eps and .png
end



i=0;
% om=10:5:250;
for om=1:5:251

i=i+1;
err1(i)=0.1/om^(2);
err3(i)=10/om^(4);
err4(i)=100/om^(5);
err6(i)=1e+4/om^7;

end

om=1:5:251;
% semilogy(10:10:500,abs(err1),'-b',10:10:500,abs(err2),'-.c',10:10:500,abs(err3),'.g',10:10:500,abs(err4),'.y',10:10:500,abs(err5),'-.r')%,10:10:500,abs(err6),'--k')
% semilogy(om,abs(err1),'-.c',om,abs(err2),'-.k',om,abs(err3),'.g',om,abs(err5),'-.r')
semilogy(om,abs(err1),'-',om,abs(err3),'--',om,abs(err4),'-.',om,abs(err6),'-')
% legend('one','two','three','five')
legend('s=1','s=3','s=4','s=6')
axis([0 251 1e-15 1e+5])
xlabel('omega [frequency]')
ylabel('order of the residual')
title('Error from the asymptotic expansion')