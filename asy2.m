close all
clear all

% this sucks, let's not use it

fg=@(x,om) cos(x).*exp(om*(x.^2+x)*1.0i); 
f=@(x) cos(x);
g=@(x) x.^2+x;
gp1=@(x) 2*x+1;

s1=@(x) f(x)/gp1(x);
s2=@(x) -(2*cos(x)+sin(x)+2*x*sin(x))/((1+2*x)^3);
s3=@(x) ((11-4*x*(1+x))*cos(x)+6*(1+2*x)*sin(x))/((1+2*x)^5);
s4=@(x) (12*(-9+4*x*(1+x))*cos(x)+(1+2*x)*sin(x))/((1+2*x)^7);
s5=@(x) ((1501+8*x*(1+x)*(-89+2*x*(1+x)))*cos(x)-20*(1+2*x)*(-41+4*x*(1+x))*sin(x))/((1+2*x)^9);
s6=@(x) -(30*(897+8*x*(1+x)*(-55+2*x*(1+x)))*cos(x)+(1+2*x)*(14701+8*x*(1+x)*(-209+2*x*(1+x)))*sin(x))/(1+2*x)^11;

s7=@(x) ((590519-4*x*(1+x)*(73923+4*x*(1+x)*(-837+4*x*(1+x))))*cos(x)+42*(1+2*x)*(7681+8*x*(1+x)*(-119+2*x*(1+x)))*sin(x))/((1+2*x)^13)


I=[0,1];
a=I(1);
b=I(2);

sol1=0.0114597001513076719+0.0546524663174186983i;

for om=20

% om=10;
om
% fg1=@(x) fg(x,om);
sol1=quadgk(@(x) fg(x,om),0,1,'AbsTol',1e-9,'RelTol',1e-9);

solasy1=-(s1(b)*exp(om*g(b)*1.0i) -s1(a)*exp(om*g(a)*1.0i))/(-(om*1.0i));

solasy2=solasy1-(s2(b)*exp(om*g(b)*1.0i)-s2(a)*exp(om*g(a)*1.0i))/(-(om*1.0i)^2);

solasy3=solasy2-(s3(b)*exp(om*g(b)*1.0i)-s3(a)*exp(om*g(a)*1.0i))/(-(om*1.0i)^3);

solasy4=solasy3-(s4(b)*exp(om*g(b)*1.0i)-s4(a)*exp(om*g(a)*1.0i))/(-(om*1.0i)^4);

solasy5=solasy4-(s5(b)*exp(om*g(b)*1.0i)-s5(a)*exp(om*g(a)*1.0i))/(-(om*1.0i)^5);

solasy6=solasy5-(s6(b)*exp(om*g(b)*1.0i)-s6(a)*exp(om*g(a)*1.0i))/(-(om*1.0i)^6);

solasy7=solasy6-(s7(b)*exp(om*g(b)*1.0i)-s7(a)*exp(om*g(a)*1.0i))/(-(om*1.0i)^7);

err(1)=sol1-solasy1
err(2)=sol1-solasy2
err(3)=sol1-solasy3
err(4)=sol1-solasy4
err(5)=sol1-solasy5
err(6)=sol1-solasy6
err(7)=sol1-solasy7

%%%%%%%%%%%%%% second plot, error in relation to number of asymptotic steps
%%%%%%%%%%%%%% this suuuuuuuucksssssss
end

semilogy(1:7,abs(err))

% legend('s=1','s=3','s=4','s=6')
% axis([0 251 1e-15 1e+5])
xlabel('s-term asymptotic expansion')
ylabel('error')
title('Error from the asymptotic expansion')