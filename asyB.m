function [err]=asyB(Om,psi)
a=0; b=1;
f=@(x) sinh(x);
g=@(x) x.^3+x.^2+x;
i=0;
for om=Om
i=i+1;
sol1=quadgk(@(x) f(x).*exp(1.0i*om*g(x)),0,1,'AbsTol',1e-14,'RelTol',2.23e-15,'MaxIntervalCount',10000);

solasy1=-(psi(b,1)*exp(om*g(b)*1.0i) -psi(a,1)*exp(om*g(a)*1.0i))/(-(om*1.0i));

solasy2=-(psi(b,2)*exp(om*g(b)*1.0i)-psi(a,2)*exp(om*g(a)*1.0i))/(-(om*1.0i)^2);

%solasy3=solasy2-(s3(b)*exp(om*g(b)*1.0i)-s3(a)*exp(om*g(a)*1.0i))/(-(om*1.0i)^3);

err(i)=abs(sol1-solasy2);
end

end