function [I] = asymptotics(s,W,f,g) 



a = 0;
b = 1;
% x = linspace(a,b,10000);
% 
% ff = @(x) cos(x);
% gg = @(x) (x.^2 + x);


syms t;
df = diff(f,'t');
dg = diff(g,'t');
sigma{1} = f ./ dg;
for k=1:s
    dsigma{k} = simplify(diff(sigma{k},'t'));
    
    sigma{k+1} = simplify(dsigma{k} ./ dg);
end
t = a;
ga = eval(g);
t = b;
gb = eval(g);



l = length(W);
errAsym = [];


for w = W
     I = 0;    
     for i = 1:s
         t = b;
         s1 = eval(sigma{i});
         t = a;
         s2 = eval(sigma{i});
         I = I + 1./(-1i*w).^s *(s1 * exp(1i * w * gb) - ...
                                 s2* exp(1i * w * ga));
     end
     I = -I;
end

end