%% Filon method for integrating oscillatory functions

clear all;
clc;
close all;

a = 0;
b = 1;
f = @(x)( cos(x));
df = @(x) ( -sin(x));
g = @(x)( x );



M = [1 0 0 0 ; 1 1 1 1 ; 0 1 0 0 ; 0 1 2 3];
b = [f(0) f(1) df(0) df(1)]';
a = M \ b;



syms t;
ff = cos(t);
gg = t;
W = 200:1:300;
err1 = error_asymptotic(2,W,ff,gg);

v =  a(1) + a(2)*t + a(3)*t.^2 + a(4)*t.^3;
qf = ff - v;

err2 = error_asymptotic(1,W,qf,gg);


plot(W',W.^3 .*(err1), 'r');
hold on;
plot(W',W.^3 .*(err2), 'b');



