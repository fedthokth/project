%% Filon method for integrating oscillatory functions

clear all;
clc;
close all;

a = 0;
b = 1;
f = @(x)( (x + 3) ./ (x+1));
df = @(x) ( -2 ./ (x+1).^2);
g = @(x)( x );


% multiplicity = 1
% M1 = [1 0  ; 1 1 ];
% b1 = [f(0) f(1)]';
% a1 = M1 \ b1;
% 
% u = [0 0.5 1]';
% 
% M12 = [ones(3,1)  u  u.^2 ];
% b12 = f(u);
% a12 = M12 \ b12;


% multiplicity = 2
M2 = [1 0 0 0 ; 1 1 1 1 ; 0 1 0 0 ; 0 1 2 3];
b2 = [f(0) f(1) df(0) df(1)]';
a2 = M2 \ b2;

u = [0 0.5 1]';

fM22 =  [ones(3,1)   u         u.^2  u.^3    u.^4 u.^5];
dM22 = [zeros(3,1)  ones(3,1) 2.*u 3.*u.^2  4*u.^3  5*u.^4];
M22 = [fM22 ; dM22]; 

b22 = [f(u) ; df(u)];
a22 = M22 \ b22;

syms t;
ff = (t + 3) / (t + 1);
gg = t;
W = 0:1:200;
err1 = error_asymptotic(2,W,ff,gg);


v =  a2(1) + a2(2)*t + a2(3)*t.^2 + a2(4)*t.^3;
qf = ff - v;
err2 = error_asymptotic(1,W,qf,gg);

v =  a22(1) + a22(2)*t + a22(3)*t.^2 + a22(4)*t.^3 + a22(5)*t.^4 + a22(6)*t.^5  ;
qf = ff - v;
err3 = error_asymptotic(1,W,qf,gg);



figure(1)
plot(W',W.^3 .*(err1), 'r');
hold on;
plot(W',W.^3 .*(err2), 'b');
hold on;
plot(W',W.^3 .*(err3), 'k');


