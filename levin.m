
function [err] = levin(Om,psi,Dpsi)

%   basis: polynomials
%   nodes: 0,1
%   mutiplicities: 1,1 (standard)
a=0; b=1;
f=@(x) sinh(x);
g=@(x) x.^3+x.^2+x;
Dg=@(x) 3*x.^2+2*x+1;

% om=20;
i=0;
for om=Om
i=i+1;
sol=quadgk(@(x) f(x).*exp(1.0i*om*g(x)),0,1,'AbsTol',1e-14,'RelTol',2.23e-15,'MaxIntervalCount',10000);

x=[a;b];
m=[1;1];
n=sum(m)-1;

A=zeros(n+1,n+1);
B=zeros(n+1,1);

for k=1:n+1
    A(k,1)=Dpsi(x(k),0)+1.0i*om*Dg(x(k))*psi(x(k),0);
    A(k,2)=Dpsi(x(k),1)+1.0i*om*Dg(x(k))*psi(x(k),1);
    B(k)=f(x(k));
end

c=A\B;

Q=exp(1.0i*om*g(b))*(c(1)*psi(b,0)+c(2)*psi(b,1))-exp(1.0i*om*g(a))*(c(1)*psi(a,0)+c(2)*psi(a,1));

% sol
% Q
err(i)=abs(sol-Q);

end

% om=1:200;

% err2=err.*(Om.^2);


% plot(om,err2)

% for i=1:length(m)
%     for j=1:m(i)
%         for k=1:n+1
%             A(,)
%         end
%     end 
% end

end