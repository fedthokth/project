
function [err] = levin2(Om,psi,Dpsi)

%   basis: polynomials
%   nodes: 0,1/2,1
%   mutiplicities: 1,1,1 (standard)
a=0; b=1;
f=@(x) sinh(x);
g=@(x) x.^3+x.^2+x;
Dg=@(x) 3*x.^2+2*x+1;

% om=20;
i=0;
for om=Om
i=i+1;
sol=quadgk(@(x) f(x).*exp(1.0i*om*g(x)),0,1,'AbsTol',1e-14,'RelTol',2.23e-14,'MaxIntervalCount',200000);

x=[a;(a+b)/2;b];
m=[1;1;1];
n=sum(m)-1;

A=zeros(n+1,n+1);
B=zeros(n+1,1);

for k=1:n+1
    for l=1:n+1
        A(k,l)=Dpsi(x(k),l-1)+1.0i*om*Dg(x(k))*psi(x(k),l-1);
%     A(k,2)=Dpsi(x(k),1)+1.0i*om*Dg(x(k))*psi(x(k),1);
    end
    B(k)=f(x(k));
end
% A
% B

c=A\B;

Q=exp(1.0i*om*g(b))*(c(1)*psi(b,0)+c(2)*psi(b,1)+c(3)*psi(b,2))-exp(1.0i*om*g(a))*(c(1)*psi(a,0)+c(2)*psi(a,1)+c(3)*psi(a,2));

% sol
% Q
err(i)=abs(sol-Q);

end

% om=1:200;

% err2=err.*(Om.^ord);


% plot(om,err2)

% for i=1:length(m)
%     for j=1:m(i)
%         for k=1:n+1
%             A(,)
%         end
%     end 
% end

end