function [err] = levin4(Om,psi,Dpsi,D2psi)
err=zeros(1,length(Om));
%   basis: polynomials
%   nodes: 0,1/2,1
%   mutiplicities: 1,1,1 (standard)
a=0; b=1;
f=@(x) sinh(x);
g=@(x) x.^3+x.^2+x;
Dg=@(x) 3*x.^2+2*x+1;
D2g=@(x) 6*x+2;
Df=@(x) cosh(x);

% om=20;
i=0;
for om=Om
i=i+1;
sol=quadgk(@(x) f(x).*exp(1.0i*om*g(x)),0,1,'AbsTol',1e-14,'RelTol',2.23e-14,'MaxIntervalCount',200000);

x=[a;(a+b)/2;b];
m=[2;1;2];
n=sum(m)-1;

A=zeros(n+1,n+1);
B=zeros(n+1,1);

for l=1:n+1
    A(1,l)=Dpsi(x(1),l-1)+1.0i*om*Dg(x(1))*psi(x(1),l-1);
    A(2,l)=D2psi(x(1),l-1)+1.0i*om*D2g(x(1))*psi(x(1),l-1)+1.0i*om*Dg(x(1))*Dpsi(x(1),l-1);
    A(3,l)=Dpsi(x(2),l-1)+1.0i*om*Dg(x(2))*psi(x(2),l-1);
    A(4,l)=Dpsi(x(3),l-1)+1.0i*om*Dg(x(3))*psi(x(3),l-1);
    A(5,l)=D2psi(x(3),l-1)+1.0i*om*D2g(x(3))*psi(x(3),l-1)+1.0i*om*Dg(x(3))*Dpsi(x(3),l-1);
end
B(1)=f(x(1));
B(2)=Df(x(1));
B(3)=f(x(2));
B(5)=Df(x(3));
B(4)=f(x(3));
c=A\B;
fun1=0.;
fun2=0.;
for j=1:n+1
    fun1=fun1+c(j)*psi(b,j-1);
    fun2=fun2+c(j)*psi(a,j-1);
end

Q=exp(1.0i*om*g(b))*fun1-exp(1.0i*om*g(a))*fun2;

% sol
% Q
err(1,i)=abs(sol-Q);

end

end