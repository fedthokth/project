%% Classical numerical integration for integrating oscillatory functions

clear all;
clc;


a = 0;
b = 1;
x = linspace(a,b,100);
f = @(x)( x.^2);
g = @(x)( x );



W = 10:5:500;
l = length(W);
errorTr = [];
errorGL = [];

for w = W
    
    h = @(x)(f(x) .* exp(1i * w .* g(x)));
    
    % trapezoide rule
    xq = linspace(a,b,25);
    yq = (h(xq));
    Itrap = trapz(xq,yq);
    
    % Gauss-Legendre quadrature rule       
    Igl = gaussLegendre(h, a, b, 10);
    
    
    % save the error
    errorTr = [errorTr; abs(Itrap) ];
    errorGL = [errorGL; abs(Igl) ];
end
figure(1)
plot(W',errorTr);
figure(2)
plot(W',errorGL);