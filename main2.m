%% 
%  Asymptotics expansion for integrating oscillatory function
%  No stationary points

clear all;
clc;
clf;
syms t;
f =  cos(t);
g = t.^2 + t ;
h = simplify(f*exp(1i .* g));

Iex = double(int(h,0,1));



W = 1:1:250;

err = abs(asymptotics(1,1,f,g) - Iex);
err = err * 1./ (W.^2);    
figure(1)
plot(W',log10(err), 'b.-');
hold on;

err = abs(asymptotics(2,1,f,g) - Iex);
err = err * 1./ (W.^3);   
plot(W',log10(err), 'ro-');


err = abs(asymptotics(10,1,f,g) - Iex)
err = err * 1./ (W.^11);   
plot(W',log10(err), 'k');


% W = 20;
% err = [];
% for k = 1:10
%     err = [err asymptotics(k,W,f,g)];      
% end
% semilogy([1:10],abs(err))


