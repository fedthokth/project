%plots 1 and 2
close all
clear all
w=1:3:500;

err12=asy(w,@psi3);
err12=err12.*(w.^2);

err1=levin(w,@psi1,@Dpsi1); %polynomials
err2=levin(w,@psi4,@Dpsi4); %trigonometric

err1=err1.*(w.^2);
err2=err2.*(w.^2);

err3=levin2(w,@psi1,@Dpsi1); %polynomials
err4=levin2(w,@psi4,@Dpsi4); %trigonometric

err3=err3.*(w.^2);
err4=err4.*(w.^2);

plot(w,err12,'-',w,err1,'-',w,err2,'-',w,err3,'-',w,err4,'-')
xlabel('w=omega')
ylabel('error * w^2')
title('Error scaled with w^2')
legend('1-term asymptotic','polynomial degree 1','trigonometric degree 1','polynomial degree 2','trigonometric degree 2')

