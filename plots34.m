%plots 3 and 4
close all
clear all
w=1:3:500;

% err12=asyB(w,@psi3);
% err12=err12.*(w.^3);

err1=levin3(w,@psi1,@Dpsi1,@D2psi1); %polynomials
err2=levin3(w,@psi4,@Dpsi4,@D2psi4); %trigonometric

err1=err1.*(w.^3);
err2=err2.*(w.^3);

err12=levin(w,@psi3,@Dpsi3);
err12=err12.*(w.^3);

err3=levin4(w,@psi1,@Dpsi1,@D2psi1); %polynomials
err4=levin4(w,@psi4,@Dpsi4,@D2psi4); %trigonometric

err3=err3.*(w.^3);
err4=err4.*(w.^3);

plot(w,err1,'-',w,err2,'-',w,err12,'-',w,err3,'-',w,err4,'-')
% plot(w,err1,'-',w,err2,'-')
xlabel('w=omega')
ylabel('error * w^3')
title('Error scaled with w^3')
legend('polynomial degree 3','trigonometric degree 3','sigma_k degree 1','polynomial degree 4','trigonometric degree 4')
% legend('polynomial degree 3','trigonometric degree 3')
