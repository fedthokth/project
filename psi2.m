function [ y ] = psi2( x,k )
%family of funtions, basis for the space
%   fisrt case: polynomials
if k==0
    y=1;
elseif k==1
    y=cos(x);
elseif k==2
    y=1.0i*sin(x);
elseif k==3
    y=cos(2*x);
elseif k==4
    y=1.0i*sin(2*x);
else
    y=cos(3*x);
end
end

