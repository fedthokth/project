function [ y ] = psi3( x,k )
%family of funtions, basis for the space
%   fisrt case: polynomials
if k==0
    y=1;
elseif k==1
    y=sinh(x)/(3*x^2+2*x+1);
elseif k==2
    y=(cosh(x)+x*(2+3*x)*cosh(x)-2*(1+3*x)*sinh(x))/((1+x*(2+3*x))^3);
elseif k==3
    y=(-6*(1+x*(5+9*x*(1+x)))*cosh(x)+(7+x*(2+3*x))*(32+x*(2+3*x))*sinh(x))/((1+x*(2+3*x))^5);
elseif k==4
    y=(-20*(1+3*x)*(1+x*(2+3*x))*(7+x*(2+3*x)*(92+x*(2+3*x)))*cosh(x)+(-359+x*(2+3*x)*(1444+x*(2+3*x)*(9006+x*(2+3*x)*(484+x*(2+3*x)))))*sinh(x))/(1+x*(2+3*x))^9;
end
end

