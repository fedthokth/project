function [ y ] = psi4( x,k )
%family of funtions, basis for the space
%   fisrt case: polynomials
if k==0
    y=1;
elseif k==1
    y=cos(x)+1.0i*sin(x);
elseif k==2
    y=cos(x)-1.0i*sin(x);
elseif k==3
    y=cos(2*x)+1.0i*sin(2*x);
elseif k==4
    y=cos(2*x)-1.0i*sin(2*x);
end
end

